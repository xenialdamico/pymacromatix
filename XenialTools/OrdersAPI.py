# Keep this here, we may need to retrieve an Order, update it... then send it back through Pipeline again with current date/time info.

import requests,json

debug = 1

def getLastOrderData(ordersconn, token, date, companyid, sites):
    headers = {
        'authorization': token,
        'hc-company-id': companyid,
        'hc-site-id': sites,
        'Content-Type': 'application/json',
        'Connection': "keep-alive",
        'cache-control': "no-cache"
        }
    #Default
    strURL = "/Order?$top=1&$orderby=time/closed%20desc"

    # Contains Combo for LJS
    #strURL = "/Order?$filter=_id%20eq%20b15c9a63-9f6e-4e5d-b649-9531078ecaf5"
    # Complex order for LJS
    #strURL = "/Order?$filter=order_number%20eq%20'500135'&$orderby=time/closed%20desc&$top=1"
    # Refund order for LJS
    # strURL = "/Order?$filter=order_type%20eq%20'refund'&$orderby=time/closed%20desc&$top=1"
    # Order with Change for LJS
    # strURL = "/Order?$filter=order_number%20eq%20'500009'&$orderby=time/closed%20desc&$top=1"
    # Voided Post Payment Order for LJS
    #strURL = "/Order?$filter=order_number%20eq%20'500008'&$orderby=time/closed%20desc&$top=1"
    
    #-----------------------------------------------------
    # Generic Order Queries, which work for any company.
    # Last Discounted Order
    # strURL = "/Order?$filter=discount_total%20ge%200.01&$orderby=time/closed%20desc&$top=1"

    #Last Order Paid with a Credit Card
    # strURL = "/Order?$filter=payment_info/payments/code%20eq%20'CREDIT'&$orderby=time/closed%20desc&$top=1"

    # Last Order with a Modifier
    #strURL = "/Order?$orderby=time/closed%20desc&$filter=items/child_items/item_type eq 'modifier'&$top=1"
    

    ordersURL = ordersconn + strURL
    res = requests.request("GET", ordersURL, headers=headers)

    if res.status_code != 200:
        print(("ERROR GETTING ORDER DATA: Status Code returned of " + str(res.status_code)))
        print("Retrying...")
        res = requests.request("GET", ordersURL, headers=headers)
        if res.status_code != 200:
            print(("ERROR GETTING ORDER DATA: Status Code returned of " + str(res.status_code)))
            exit()
        else:
            orderResJSON = json.loads(orderResObj)
            if debug==1:
                print("Response JSON:" + json.dumps(orderResJSON, indent=4, sort_keys=True))
            return orderResObj
    else:
        orderResObj = res.text

        orderResJSON = json.loads(orderResObj)
        if debug==1:
            print("Response JSON:" + json.dumps(orderResJSON, indent=4, sort_keys=True))
        return orderResObj


def getLastOrderData(ordersconn, token, date, companyid, sites):
    headers = {
        'authorization': token,
        'hc-company-id': companyid,
        'hc-site-id': sites,
        'Content-Type': 'application/json',
        'Connection': "keep-alive",
        'cache-control': "no-cache"
        }
    #Default
    strURL = "/Order?$orderby=time/closed%20desc&$top=50&$filter=order_source ne 'DAmico'"

    # Contains Combo for LJS
    #strURL = "/Order?$filter=_id%20eq%20b15c9a63-9f6e-4e5d-b649-9531078ecaf5"
    # Complex order for LJS
    #strURL = "/Order?$filter=order_number%20eq%20'500135'&$orderby=time/closed%20desc&$top=1"
    # Refund order for LJS
    # strURL = "/Order?$filter=order_type%20eq%20'refund'&$orderby=time/closed%20desc&$top=1"
    # Order with Change for LJS
    # strURL = "/Order?$filter=order_number%20eq%20'500009'&$orderby=time/closed%20desc&$top=1"
    # Voided Post Payment Order for LJS
    #strURL = "/Order?$filter=order_number%20eq%20'500008'&$orderby=time/closed%20desc&$top=1"
    
    #-----------------------------------------------------
    # Generic Order Queries, which work for any company.
    # Last Discounted Order
    # strURL = "/Order?$filter=discount_total%20ge%200.01&$orderby=time/closed%20desc&$top=1"

    #Last Order Paid with a Credit Card
    # strURL = "/Order?$filter=payment_info/payments/code%20eq%20'CREDIT'&$orderby=time/closed%20desc&$top=1"

    # Last Order with a Modifier
    #strURL = "/Order?$orderby=time/closed%20desc&$filter=items/child_items/item_type eq 'modifier'&$top=1"
    

    ordersURL = ordersconn + strURL
    res = requests.request("GET", ordersURL, headers=headers)

    if res.status_code != 200:
        print(("ERROR GETTING ORDER DATA: Status Code returned of " + str(res.status_code)))
        print("Retrying...")
        res = requests.request("GET", ordersURL, headers=headers)
        if res.status_code != 200:
            print(("ERROR GETTING ORDER DATA: Status Code returned of " + str(res.status_code)))
            exit()
        else:
            orderResJSON = json.loads(orderResObj)
            if debug==1:
                print("Response JSON:" + json.dumps(orderResJSON, indent=4, sort_keys=True))
            return orderResObj
    else:
        orderResObj = res.text

        orderResJSON = json.loads(orderResObj)
        if debug==1:
            print("Response JSON:" + json.dumps(orderResJSON, indent=4, sort_keys=True))
        return orderResObj