import json,requests

def getURL(key,env):
    headers = {
        'Connection': "keep-alive",
        'cache-control': "no-cache"
        }
    ssrURL = "https://ssr-xenial.heartlandcommerce.com/?env=" + env
    res = requests.request("GET", ssrURL, headers=headers)
    ssrResObj = res.text
    ssrResJSON = json.loads(ssrResObj)
    url = ""
    for item in ssrResJSON:
        if item['key'] == key:
            url = item['url']

    return url