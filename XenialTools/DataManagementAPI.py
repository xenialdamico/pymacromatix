import requests,json

debug = 0

def getSiteInfo(xprtconn, token, companyid, siteid):
    headers = {
        'authorization': token,
        'x-company-id': companyid,
        'Content-Type': 'application/json',
        'Connection': "keep-alive",
        'cache-control': "no-cache"
        }
    #Default
    strURL = "/companies/" + companyid + "/sites/" + siteid

    portalURL = xprtconn + strURL
    res = requests.request("GET", portalURL, headers=headers)

    if res.status_code != 200:
        print(("ERROR GETTING MODIFIER GROUP DATA: Status Code returned of " + str(res.status_code)))
        exit()
    sitesResObj = res.text

    sitesResJSON = json.loads(sitesResObj)
    if debug==1:
        print("Response JSON for Sites:" + json.dumps(sitesResJSON, indent=4, sort_keys=True))
        print("----------------------------------------------")
    return sitesResObj



def getVariantByEntityId(dmURL, token, companyid, sites, entityId, date):
    headers = {
        'authorization': token,
        'x-company-id': companyid,
        'x-site-ids': sites,
        'Content-Type': 'application/json',
        'Connection': "keep-alive",
        'cache-control': "no-cache"
        }

    strURL = "/variant/current?entity_ids=['" + entityId  + "']&effective_date=" + date + "T00:00:00.000Z"

    dmURL = dmURL + strURL
    res = requests.request("GET", dmURL, headers=headers)

    if res.status_code != 200:
        print(("ERROR GETTING VARIANT DATA: Status Code returned of " + str(res.status_code)))
        exit()
    variantResObj = res.text

    variantResJSON = json.loads(variantResObj)
    if debug==1:
        print("Response JSON for Variant:" + json.dumps(variantResJSON, indent=4, sort_keys=True))
        print("----------------------------------------------")
    return variantResJSON

def getModifierGroupByEntityId(dmURL, token, companyid, sites, entityId, date):
    headers = {
        'authorization': token,
        'x-company-id': companyid,
        'x-site-ids': sites,
        'Content-Type': 'application/json',
        'Connection': "keep-alive",
        'cache-control': "no-cache"
        }

    strURL = "/modifier-group/current?entity_ids=['" + entityId  + "']&effective_date=" + date + "T00:00:00.000Z"

    dmURL = dmURL + strURL
    res = requests.request("GET", dmURL, headers=headers)

    if res.status_code != 200:
        print(("ERROR GETTING MODIFIER GROUP DATA: Status Code returned of " + str(res.status_code)))
        exit()
    modifierGroupResObj = res.text

    modifierGroupResJSON = json.loads(modifierGroupResObj)
    if debug==1:
        print("Response JSON for Modifier Group:" + json.dumps(modifierGroupResJSON, indent=4, sort_keys=True))
        print("----------------------------------------------")
    return modifierGroupResJSON