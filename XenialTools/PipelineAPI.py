
import sys,requests,json
from datetime import datetime

debug = 0

def postOrder(pipeconn, site_token, company_id, site_id, arrMsgs):
    headers = {
        'authorization': site_token,
        'hc-company-id': company_id,
        'hc-site-id': site_id,
        'Content-Type': 'application/json',
        'Connection': "keep-alive",
        'cache-control': "no-cache"
        }
    
    strURL = "/ds/pipeline/pos.order"
    pipeURL = pipeconn + strURL
    if debug:
        print(arrMsgs)
    try:
        res = requests.request("POST", pipeURL, headers=headers, data=arrMsgs)
    except requests.exceptions.Timeout:
        print("ERROR! Timeout occurred!")
    # except requests.exceptions.TooManyRedirects:
        # Tell the user their URL was bad and try a different one
    except requests.exceptions.RequestException as e:
        # catastrophic error. bail.
        raise SystemExit(e) 
    
    ResObj = res.text

    ResJSON = json.loads(ResObj)
    if debug==1:
        print("Response JSON:" + str(ResJSON))

    print(datetime.today().strftime("%Y-%m-%dT%H:%M:%S") + ":    Order Posted to Pipeline(" + strURL + "). RequestID:" + ResJSON['request_id'])
    return ResObj
