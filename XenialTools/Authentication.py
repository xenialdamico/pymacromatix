import requests

debug = 1

#-------------------------------------------------------------------------------
# SITE TOKEN
# A token used to work with Pipeline.
#-------------------------------------------------------------------------------
def getSiteToken(xprtconn, site_id, access_code, companyid):
    itoken_headers = {
        'content-type': "application/json"
        }
    itoken_payload = "{\n\n  \"site\": \""+ site_id + "\",\n\n  \"access_code\": \""+ access_code + "\"\n\n}"
    if debug:
        print("Integrator Token Payload: " + itoken_payload)
    itoken_url = str(xprtconn) + "/token"
    res = requests.request("POST", itoken_url, data=itoken_payload, headers=itoken_headers)
    if res.status_code != 200:
        print(("ERROR GETTING SITE TOKEN: Status Code returned of " + str(res.status_code)))
        print(res.text)
        exit()
    itoken = res.text
    itoken = "Bearer " + itoken
    return itoken

#-------------------------------------------------------------------------------
# INTEGRATOR TOKEN
# A token used to Application to Application conversations. 
#-------------------------------------------------------------------------------
def getIntegratorToken(xprtconn, key_id, secret_key, companyid):
    
    itoken_headers = {
        'content-type': "application/json",
        'cache-control': "no-cache"
        }
    itoken_payload = "{\n\n  \"key_id\": \""+ key_id + "\",\n\n  \"secret_key\": \""+ secret_key + "\"\n\n}"
    if debug:
        print("Integrator Token Payload: " + itoken_payload)
    itoken_url = str(xprtconn) + "/integrator/token"
    res = requests.request("POST", itoken_url, data=itoken_payload, headers=itoken_headers)
    if res.status_code != 200:
        print(("ERROR GETTING ACCESS TOKEN: Status Code returned of " + str(res.status_code)))
        exit()
    itoken = res.text
    itoken = "Bearer " + itoken
    return itoken
#-------------------------------------------------------------------------------
# PERSON TOKEN
# A person token impersonates a user session. Site access may be restricted
# to the User's list of allowed sites.
#-------------------------------------------------------------------------------
def getPersonToken(xprtconn, user, password):
    authPayload = {
        'user': user,
        'password': password
        }           
    authPayload = json.dumps(authPayload)
   
    authheaders = {
        'content-type': "application/json",
        'cache-control': "no-cache"
        }
    res = requests.request("POST", xprtconn + "/token", data=authPayload, headers=authheaders)
    if res.status_code != 200:
        print(("AUTH ERROR: Problem encountered while getting a person token. Status Code returned of " + str(res.status_code)))
        exit()
    token = res.text
    token = "Bearer " + token
    return token
#-------------------------------------------------------------------------------
# ACCESS TOKEN
# An access token is somewhere in between a Person & Integrator token.
# It's only used in specific portions of the platform, currently.
#-------------------------------------------------------------------------------
def getAccessToken(xprtconn, token, companyid):
    atoken_headers = {
        'authorization': token,
        'content-type': "application/json",
        'cache-control': "no-cache"
        }
    atoken_payload = "{\"company_id\": \"" + companyid + "\"}\n"
    print(atoken_payload)
    accesstokenres = requests.request("POST", xprtconn + "/v1/me/access-token", data=atoken_payload, headers=atoken_headers)
    if accesstokenres.status_code != 200:
        print(("AUTH ERROR: Problem encountered while getting an access token. Status Code returned of " + str(accesstokenres.status_code) + ". Error Description: " + str(accesstokenres.text)))
        exit()
    accesstoken = accesstokenres.text
    accesstoken = "Bearer " + accesstoken
    return accesstoken
