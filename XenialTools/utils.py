import random, math
def GenerateOrderNumber():
    digits = [i for i in range(0, 10)]
    random_ordernum = ""

    ## create some random order number 
    for i in range(6):
        index = math.floor(random.random() * 10)
        random_ordernum += str(digits[index])
    return random_ordernum

def is_empty(any_structure):
    if any_structure:
        return False
    else:
        return True