# SAMPLE UTILITY - Encounter to XMM #
Scripts included in this utility are intended to mock Encounter Data, so that we can perform the following activities with surgical precision:
- Validation - ensuring data elements are in the correct format, in the correct place within the JSON Schema.
- Demonstration - Facilitate pre-sales discussions and pilot initiatives.
- Corrective Actions - When the Xenial team recommends a change to a data element, this toolset allows us to mock that change and watch it go through the ecosystem, prior to any dev work needs to be completed. This ensures that the advice provided is totally accurate and we can have confidence in our guidance.

### What is this repository for? ###
* Discovery & Requirements Preperation
* Initial Validation
* Demonstration
* Giggles

### How do I get set up? ###
* Install the following Libraries: requests, json, sys
* pip install [libary name]

# Command Line Arguments #
* All arguments should be passed with an equals(=) sign.
* environment - either qa or prod. Dev is not supported ;) 
* access_code - Found within the site object from Portal. Required for obtaining a Site token.
* company_id - uniquely identifies the Company. Required.
* file -  Identifies the file within the '/Orders' folder that you want to mock. The default is "Order_Credit.json", but specifying another file will override this setting.

## Sample Command ##
python3 SendOrders.py access_code=90e3d47f-7372-4c51-9843-b934a67d971c site_id=5fcffee2890eaf00077ab77e company_id=5fcffd5f6204f7000849aa08 environment=qa file=Order_PostVoid_Cash.json
python3 SendOrders.py access_code=b60328ef-f361-4788-b8cd-5efc3646b521 site_id=59bb1e3119bd8d2000f483cc company_id=59bb1df919bd8d2000f483cb environment=qa file=Order_PostVoid_Cash.json
