# ----------------------------------------------------------------
# Mock Order Data from Encounter to Pipeline.
# ----------------------------------------------------------------
# The point of this script is to be able to mock and test data as it flows through Pipeline and thru Reporting, en route to XMM.
# This allows me to pin point a problem or a potential change, mock the data to ensure it works as expected and to provide the Encounter
# team with the EXACT "right thing to do", in avoidance of the multitude of landmines that may inhibit our progress.

# AAFES Production Company/Site
# python3 SendOrders.py aafes=1 access_code=6c74e51f-f687-44aa-a712-473696daa367 site_id=5ff33922d3e08f00076fa7a6 company_id=5ff33884d6659900070bfbe5 environment=Prod file=Order_LJS.json

# QA
# python3 SendOrders.py access_code=b60328ef-f361-4788-b8cd-5efc3646b521 site_id=59bb1e3119bd8d2000f483cc company_id=59bb1df919bd8d2000f483cb environment=qa file=Order_LJS.json

import XenialTools.Authentication, XenialTools.PipelineAPI, XenialTools.utils, XenialTools.SSR
import json, sys, requests
from datetime import datetime
import random, math, uuid

debug=1

# -------------------------------------------
# Initialize Variables
# -------------------------------------------
company_id = "" 
site_id = "" 
user = ""
password = ""
environment = ""
date=""
strFile="Order_Credit.json"
#this is dumb, I should just get the company name from Portal.
aafes=1

# -------------------------------------------
# get script arguments
# -------------------------------------------
position = 1
while (position < len(sys.argv)):  
    if "=" in sys.argv[position]:
        KeyValue = sys.argv[position].split("=")
        # Check for 'Environment' Argument
        if KeyValue[0].lower() == 'environment':
            # Get Env from commandline
            environment = KeyValue[1]
            print(("Environment Arg Accepted: " + KeyValue[1]))
        # Check for 'CompanyID' Argument
        elif KeyValue[0].lower() == 'company_id':
            # Get CompanyId from commandline
            company_id = KeyValue[1]
            print(("company_id Arg Accepted: " + KeyValue[1]))
        elif KeyValue[0].lower() == 'site_id':
            # Get site_id from commandline
            site_id = KeyValue[1]
            print(("site_id Arg Accepted: " + KeyValue[1]))
        # Check for 'access_code' Argument
        elif KeyValue[0].lower() == 'access_code':
            # Get Secret from commandline
            access_code = KeyValue[1]
            print(("access_code Arg Accepted: " + KeyValue[1]))
        elif KeyValue[0].lower() == 'file':
            # Get Secret from commandline
            strFile = KeyValue[1]
            print(("file Arg Accepted: " + KeyValue[1]))
        elif KeyValue[0].lower() == 'aafes':
            # Get Secret from commandline
            aafes = KeyValue[1]
            print(("aafes Arg Accepted: " + KeyValue[1]))
    else:
        args[KeyValue[0]] = KeyValue[1]
    position += 1

# -------------------------------------------
# Setup Environment
# -------------------------------------------
if environment.lower() == "production" or environment.lower() == "prod":
    xprtconn = XenialTools.SSR.getURL("portal", "prod") 
    pipeconn = XenialTools.SSR.getURL("pl", "prod") 
    
if environment.lower() == "qa":
    xprtconn = XenialTools.SSR.getURL("portal", "qa") 
    pipeconn = XenialTools.SSR.getURL("pl", "qa") 
    
# Check for Company and Site info.
if company_id.lower() == "" or site_id.lower() == "":
    print("FATAL ERROR: Company or Site missing. Pass Argument Company_ID=<value>, and/or Site_ID=<value>")
    exit()

# -----------------------------------------------------------------
# Main Script Logic - Let's do it!
# -----------------------------------------------------------------


# Create some Date Strings.
strToday = datetime.today().strftime('%Y-%m-%d')
strNow = datetime.today().strftime("%Y-%m-%dT%H:%M:%S.000Z")

# Create a Random OrderNum
ranOrdNum = XenialTools.utils.GenerateOrderNumber()

# Get Site Token
strToken = XenialTools.Authentication.getSiteToken(xprtconn,site_id,access_code,company_id)

strFile = 'orders/' + strFile
f = open(strFile)

data = json.load(f) 

order = data['order']

# Set Props for the Order to make it current, and for the provided Company/Site
order['_id'] = str(uuid.uuid4())
order['business_date'] = strToday + "T05:00:00.000Z"
order['company_id'] = company_id
order['site_info']['company']['id'] = company_id
if aafes:
    order['site_info']['company']['name'] = "AAFES"
order['site_info']['id'] = site_id
order['time']['closed'] = strNow
for payment in order['payment_info']['payments']:
    payment['processed_at'] = strNow
order['time']['created'] = strNow
order['time']['message_created'] = strNow
order['time']['last_modified'] = strNow
order['time']['kitchen_sent'] = strNow
for item in order['items']:
    item['time']['closed'] = strNow
    item['time']['created'] = strNow
    item['time']['message_created'] = strNow

order['order_number'] = ranOrdNum
# order['order_source'] = "POS"

# So I can quickly retrieve it through Orders API
# order['origin'] = "DAmico"
order['additional_properties'] = {}
order['additional_properties']['facility_number'] = "12345"
  
strData = json.dumps(data)
f.close() 

arrMsgs = []

msgJSON={"msg_id": "","platform":"XENIAL","platform_version":"2.1.082","content_type":"application/json","msg_type":"pos.order", "payload": ""}
msgJSON['payload'] = json.dumps(data)
# Create random UUID to use for msg_id
msgJSON['msg_id'] = str(uuid.uuid4())

# Append Message and take a dumps.
arrMsgs.append(msgJSON)
arrMsgs = json.dumps(arrMsgs)
if debug:
    print(str(arrMsgs))
# Send a Message to Pipeline.
sendMsg = XenialTools.PipelineAPI.postOrder(pipeconn,strToken,company_id,site_id,arrMsgs)

# Tada!