# ----------------------------------------------------------------
# Mock Order Data from Encounter to Pipeline.
# ----------------------------------------------------------------
# The point of this script is to be able to mock and test data as it flows through Pipeline and thru Reporting, en route to XMM.
# This allows me to pin point a problem or a potential change, mock the data to ensure it works as expected and to provide the Encounter
# team with the EXACT "right thing to do", in avoidance of the multitude of landmines that may inhibit our progress.

# AAFES Production Company/Site
# python3 SendOrders_FromAPI.py key_id=683d710d-5715-467e-84b6-bed9002c9a82 secret=f9c301ef-ec56-46bc-a0d1-3a99c81c79e0 access_code=703f3327-9158-47b9-b0ee-86905ef7c357 site_id=5fae1098513f65000795f23c company_id=5faaec2ef72ee70007c94bfb environment=Prod 

# QA
# python3 SendOrders.py access_code=b60328ef-f361-4788-b8cd-5efc3646b521 site_id=59bb1e3119bd8d2000f483cc company_id=59bb1df919bd8d2000f483cb environment=qa file=Order_LJS.json

import XenialTools.Authentication, XenialTools.PipelineAPI, XenialTools.utils, XenialTools.SSR, XenialTools.OrdersAPI
import json, sys, requests
from datetime import datetime
import random, math, uuid

debug=1

# -------------------------------------------
# Initialize Variables
# -------------------------------------------
company_id = "" 
site_id = "" 
user = ""
password = ""
environment = ""
date=""
key_id=""
secret=""


aafes="0"
aafes_company_id="5ff33884d6659900070bfbe5"
aafes_site_id="5ff33922d3e08f00076fa7a6"
aafes_access_code="6c74e51f-f687-44aa-a712-473696daa367"

# -------------------------------------------
# get script arguments
# -------------------------------------------
position = 1
while (position < len(sys.argv)):  
    if "=" in sys.argv[position]:
        KeyValue = sys.argv[position].split("=")
        # Check for 'Environment' Argument
        if KeyValue[0].lower() == 'environment':
            # Get Env from commandline
            environment = KeyValue[1]
            print(("Environment Arg Accepted: " + KeyValue[1]))
        # Check for 'CompanyID' Argument
        elif KeyValue[0].lower() == 'company_id':
            # Get CompanyId from commandline
            company_id = KeyValue[1]
            print(("company_id Arg Accepted: " + KeyValue[1]))
        elif KeyValue[0].lower() == 'site_id':
            # Get site_id from commandline
            site_id = KeyValue[1]
            print(("site_id Arg Accepted: " + KeyValue[1]))
        # Check for 'access_code' Argument
        elif KeyValue[0].lower() == 'access_code':
            # Get Secret from commandline
            access_code = KeyValue[1]
            print(("access_code Arg Accepted: " + KeyValue[1]))
        elif KeyValue[0].lower() == 'file':
            # Get Secret from commandline
            strFile = KeyValue[1]
            print(("file Arg Accepted: " + KeyValue[1]))
        elif KeyValue[0].lower() == 'aafes':
            # Get Secret from commandline
            aafes = KeyValue[1]
            print(("aafes Arg Accepted: " + KeyValue[1]))
        elif KeyValue[0].lower() == 'key_id':
            # Get Secret from commandline
            key_id = KeyValue[1]
            print(("Key_ID Arg Accepted: " + KeyValue[1]))
        elif KeyValue[0].lower() == 'secret':
            # Get Secret from commandline
            secret = KeyValue[1]
            print(("Secret Arg Accepted: " + KeyValue[1]))   
    else:
        args[KeyValue[0]] = KeyValue[1]
    position += 1

# -------------------------------------------
# Setup Environment
# -------------------------------------------
if environment.lower() == "production" or environment.lower() == "prod":
    xprtconn = XenialTools.SSR.getURL("portal", "prod") 
    pipeconn = XenialTools.SSR.getURL("pl", "prod") 
    ordersconn = XenialTools.SSR.getURL("order", "prod") 
    
if environment.lower() == "qa":
    xprtconn = XenialTools.SSR.getURL("portal", "qa") 
    pipeconn = XenialTools.SSR.getURL("pl", "qa") 
    ordersconn = XenialTools.SSR.getURL("order", "qa") 
    
# Check for Company and Site info.
if company_id.lower() == "" or site_id.lower() == "":
    print("FATAL ERROR: Company or Site missing. Pass Argument Company_ID=<value>, and/or Site_ID=<value>")
    exit()

# -----------------------------------------------------------------
# Main Script Logic - Let's do it!
# -----------------------------------------------------------------


# Create some Date Strings.
strToday = datetime.today().strftime('%Y-%m-%d')
strNow = datetime.today().strftime("%Y-%m-%dT%H:%M:%S.000Z")

if aafes=="1":
    # Get Site Token for AAFES SiteID, that's where we'll send the message since we have an Endpoint configured already.
    strSiteToken = XenialTools.Authentication.getSiteToken(xprtconn,aafes_site_id,aafes_access_code,aafes_company_id)
if aafes=="0":
    strSiteToken = XenialTools.Authentication.getSiteToken(xprtconn,site_id,access_code,company_id)
print(aafes)
# Get an Integrator token for "BK-PLK" Company.
# We'll take the Orders from that Company and Pipe them over to AAFES
strIntToken = XenialTools.Authentication.getIntegratorToken(xprtconn,key_id,secret,company_id)
orders = XenialTools.OrdersAPI.getLastOrderData(ordersconn,strIntToken,strToday,company_id,site_id)
ordersJSON = json.loads(orders)
idxNum = 1

for order in ordersJSON['items'] :
    ranOrdNum = XenialTools.utils.GenerateOrderNumber()
    order['_id'] = str(uuid.uuid4())
    order['business_date'] = strToday + "T05:00:00.000Z"
    if aafes=="1":
        order['company_id'] = aafes_company_id
        order['site_info']['id'] = aafes_site_id
        order['site_info']['company']['id'] = aafes_company_id
    if aafes=="0":
        order['company_id'] = company_id
        order['site_info']['id'] = site_id
        order['site_info']['company']['id'] = company_id
    order['time']['closed'] = strNow
    for payment in order['payment_info']['payments']:
        payment['processed_at'] = strNow
    order['time']['created'] = strNow
    order['time']['message_created'] = strNow
    order['time']['last_modified'] = strNow
    order['time']['kitchen_sent'] = strNow


    order['order_number'] = ranOrdNum
    order['order_source'] = "DAmico"
    
    order['additional_properties'] = {}
    order['additional_properties']['facility_number'] = "12362"

    arrMsgs = []

    msgJSON={"msg_id": "","platform":"XENIAL","platform_version":"2.1.082","content_type":"application/json","msg_type":"pos.order", "payload": ""}
    msgJSON['payload']= '{"order":' + json.dumps(order) + '}'

    # Create random UUID to use for msg_id
    msgJSON['msg_id'] = str(uuid.uuid4())

    # Append Message and take a dumps.
    arrMsgs.append(msgJSON)
    arrMsgs = json.dumps(arrMsgs)
    if debug:
        print(str(arrMsgs))
    # Send a Message to Pipeline.
    

    if aafes=="1":
        print(datetime.today().strftime("%Y-%m-%dT%H:%M:%S") + ":    Posting order '" + str(order['_id']) + "' to AAFES Company: " + company_id + ", SiteID: " + site_id)
        # Post the message to the AAFES Company/Site, since that's where we've configured the Data Stream endpoint.
        sendMsg = XenialTools.PipelineAPI.postOrder(pipeconn,strSiteToken,aafes_company_id,aafes_site_id,arrMsgs)
    if aafes=="0":
        print(datetime.today().strftime("%Y-%m-%dT%H:%M:%S") + ":    Posting order '" + str(order['_id']) + "' to RBI-PLK Company: " + company_id + ", SiteID: " + site_id)
        sendMsg = XenialTools.PipelineAPI.postOrder(pipeconn,strSiteToken,company_id,site_id,arrMsgs)
        idxNum = idxNum + 1
# Tada!